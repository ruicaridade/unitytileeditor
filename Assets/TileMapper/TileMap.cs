﻿using System;
using UnityEngine;

public class TileMap : MonoBehaviour {
    public int Width                    = 16;
    public int Height                   = 16;
    public float CellScale              = 1.0f;
    public int SpriteScale              = 32;
    public int GridScale                = 1;
    public GameObject SpritePrefab;

    private GameObject[,] tiles;

    public void FreezeTiles()
    {
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                if (tiles[i, j] != null)
                {
                    tiles[i, j].transform.position = new Vector2(
                        transform.position.x + i * CellScale + 0.5f * CellScale,
                        transform.position.y + j * CellScale + 0.5f * CellScale);

                    tiles[i, j].transform.rotation = Quaternion.identity;
                    tiles[i, j].transform.localScale = Vector3.one;
                }
            }
        }
    } 

    public void PlaceTile(int x, int y, Sprite sprite)
    {
        if (x >= tiles.GetLength(0) || 
            x < 0 ||
            y >= tiles.GetLength(1) ||
            y < 0)
        {
            return;
        }

        if (tiles[x, y] == null)
        {
            tiles[x, y] = (GameObject)Instantiate(SpritePrefab,
                new Vector2(
                    transform.position.x + x * CellScale + 0.5f * CellScale,
                    transform.position.y + y * CellScale + 0.5f * CellScale),
                Quaternion.identity);

            tiles[x, y].transform.parent = gameObject.transform;
        }

        tiles[x, y].GetComponent<SpriteRenderer>().sprite = sprite;
    }

    public void ApplyResize()
    {
        if (tiles != null)
        {
            int previousWidth = tiles.GetLength(0);
            int previousHeight = tiles.GetLength(1);

            GameObject[,] backup = new GameObject[previousWidth, previousHeight];
            Array.Copy(tiles, backup, previousWidth * previousHeight);
            tiles = new GameObject[Width, Height];

            int w = Math.Min(Width, previousWidth);
            int h = Math.Min(Height, previousHeight);
            
            Array.Copy(backup, tiles, w * h);

            for (int x = Width; x < previousWidth; x++)
            {
                for (int y = Height; y < previousHeight; y++)
                {
                    DestroyImmediate(backup[x, y]);
                }
            }
        }
        else
        {
            tiles = new GameObject[Width, Height];
        }
    }

    public void ClearAll()
    {
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                DestroyImmediate(tiles[i, j]);
                tiles[i, j] = null;
            }
        }
    }
}
