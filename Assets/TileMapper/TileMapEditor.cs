﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(TileMap))]
public class TileMapEditor : Editor {
    private SerializedProperty spritePrefab;
    private SerializedProperty width;
    private SerializedProperty height;
    private SerializedProperty cellScale;
    private SerializedProperty spriteScale;
    private SerializedProperty gridScale;

    private Sprite selectedSprite;
    private Texture selectedTexture;
    private List<Sprite> sprites = new List<Sprite>();
    private TileMap tileMap;
    private float xpos, ypos, xlocalpos, ylocalpos;
    private int xindex, yindex;
    private bool brushSelectionVisible;

    private bool previousPainting;
    private bool painting;
    
    private void OnEnable()
    {
        spritePrefab = serializedObject.FindProperty("SpritePrefab");
        width = serializedObject.FindProperty("Width");
        height = serializedObject.FindProperty("Height");
        cellScale = serializedObject.FindProperty("CellScale");
        gridScale = serializedObject.FindProperty("GridScale");
        spriteScale = serializedObject.FindProperty("SpriteScale");

        selectedTexture = null;
        selectedSprite = null;
        sprites.Clear();
        sprites.AddRange(Resources.LoadAll<Sprite>("TileMapResources"));

        tileMap = (TileMap)target;
        tileMap.ApplyResize();
    }
    
    private void OnSceneGUI()
    {
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

        TileMap tileMap = (TileMap)target;

        EditorGUI.BeginChangeCheck();

        for (int x = 0; x < tileMap.Width + 1; x++)
        {
            for (int y = 0; y < tileMap.Height + 1; y++)
            {
                Vector2 t = tileMap.transform.position;
                float xscaled = x * tileMap.CellScale + t.x;
                float yscaled = y * tileMap.CellScale + t.y;
                
                Handles.DrawDottedLine(new Vector3(xscaled, t.y, 0), new Vector3(xscaled, yscaled, 0), tileMap.GridScale);
                Handles.DrawDottedLine(new Vector3(t.x, yscaled, 0), new Vector3(xscaled, yscaled, 0), tileMap.GridScale);
            }
        }
        
        Vector2 mousePosition = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
        Vector2 mousePositionLocal = mousePosition - 
            new Vector2(tileMap.transform.position.x, tileMap.transform.position.y);

        xpos = mousePosition.x;
        ypos = mousePosition.y;
        xlocalpos = mousePositionLocal.x;
        ylocalpos = mousePositionLocal.y;
        xindex = Mathf.FloorToInt(mousePositionLocal.x);
        yindex = Mathf.FloorToInt(mousePositionLocal.y);

        tileMap.FreezeTiles();

        previousPainting = painting;
        painting = Event.current.button == 0 
            && Event.current.type == EventType.MouseDown 
            || previousPainting;

        if (Event.current.button == 0 && Event.current.type == EventType.MouseUp)
        {
            painting = false;
        }

        if (painting)
        {
            if (selectedSprite != null)
                tileMap.PlaceTile(xindex, yindex, selectedSprite);
        }

        SceneView.RepaintAll();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(spritePrefab);
        EditorGUILayout.PropertyField(width);
        EditorGUILayout.PropertyField(height);
        EditorGUILayout.PropertyField(cellScale);
        EditorGUILayout.PropertyField(spriteScale);
        EditorGUILayout.IntSlider(gridScale, 1, 6);
        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("Apply Changes"))
        {
            tileMap.ApplyResize();
        }

        tileMap.Width = Mathf.Clamp(tileMap.Width, 1, 1000);
        tileMap.Height = Mathf.Clamp(tileMap.Height, 1, 1000);

        EditorGUILayout.LabelField("Index: " + xindex + ", " + yindex);
        EditorGUILayout.LabelField("Position (World): " + xpos + ", " + ypos);
        EditorGUILayout.LabelField("Position (Local): " + xlocalpos + ", " + ylocalpos);

        tileMap.CellScale = Mathf.Clamp(tileMap.CellScale, 0.01f, 1000);
        tileMap.SpriteScale = Mathf.Clamp(tileMap.SpriteScale, 1, 1000);

        if (tileMap.CellScale < 0.01f)
        {
            tileMap.CellScale = 0.01f;
        }
        
        EditorGUILayout.Space();

        if (GUILayout.Button("Load Assets"))
        {
            selectedTexture = null;
            selectedSprite = null;
            sprites.Clear();
            sprites.AddRange(Resources.LoadAll<Sprite>("TileMapResources"));
        }

        brushSelectionVisible = EditorGUILayout.Foldout(brushSelectionVisible, "Brush");
        if (brushSelectionVisible)
        {
            int index = 0;
            for (int y = 0; y < sprites.Count / 4; y++)
            {
                EditorGUILayout.BeginHorizontal();

                for (int x = 0; x < 4; x++)
                {
                    Sprite s = sprites[index];
                    Texture2D tex = new Texture2D(tileMap.SpriteScale, tileMap.SpriteScale);
                    tex.SetPixels(s.texture.GetPixels((int)s.rect.x,
                        (int)s.rect.y,
                        (int)s.rect.width,
                        (int)s.rect.height));
                    tex.Apply();

                    if (GUILayout.Button(tex))
                    {
                        selectedSprite = s;
                        selectedTexture = tex;
                    }

                    index++;
                }

                EditorGUILayout.EndHorizontal();
            }
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("Clear All"))
        {
            tileMap.ClearAll();
        }
        
        EditorUtility.SetDirty(target);
    }
}
